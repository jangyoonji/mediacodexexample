package com.yoonji.snow.mediacodecexample;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.File;

/**
 * Created by snow on 2017-07-26.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class VideoDecoderActivity extends Activity implements SurfaceHolder.Callback {

    private final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private VideoDecoderThread mVideoDecoder;
    private AudioDecoderThread mAudioDecoder;

    private static final String[] FILE_PATH =
            {
                    "/storage/emulated/0/DCIM/Camera/20170807_204348.mp4",
                    "/storage/emulated/0/DCIM/Camera/20170807_204348.mp4"

            };

    //private static final String FILE_PATH = "/storage/emulated/0/DCIM/Video Editor/B612_20170621_150248_1.mp4";
    //private static final String FILE_PATH[] = {"/storage/emulated/0/KakaoTalkDownload/KakaoTalk_Video_20160629_1631_24_484.mp4"};

    private final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 100;
    private VideoEncoderThread mVideoEncoder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        SurfaceView surfaceView = new SurfaceView(this);
        surfaceView.getHolder().addCallback(this);
        setContentView(surfaceView);
        mVideoDecoder = new VideoDecoderThread();
        mAudioDecoder = new AudioDecoderThread();

    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,	int height) {
        if (mVideoDecoder != null) {
            mVideoDecoder.close();
            Log.d("TAG", "VideoDecoder destroyed1");

            mVideoDecoder = new VideoDecoderThread();
            //check premission
            if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }

            //start VideoDecoderThread
            if (mVideoDecoder.init(holder.getSurface(), FILE_PATH)) {

                mAudioDecoder.init(FILE_PATH);
                mVideoDecoder.start();
                mAudioDecoder.start();


            } else {
                mVideoDecoder = null;
                mAudioDecoder = null;
            }
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mVideoDecoder != null) {
            mVideoDecoder.close();
            Log.d("VideoDecoderActivity", "surfaceDestroyed, VideoDecoder destroyed");
        }
    }
}
