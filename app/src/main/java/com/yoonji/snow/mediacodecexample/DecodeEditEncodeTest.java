package com.yoonji.snow.mediacodecexample;

/**
 * Created by snow on 2017-07-28.
 */

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.opengl.GLES20;
import android.os.Environment;
import android.test.AndroidTestCase;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import static junit.framework.Assert.fail;


/**
 * This test has three steps:
 * <ol>
 *   <li>Generate a video test stream.
 *   <li>Decode the video from the stream, rendering frames into a SurfaceTexture.
 *       Render the texture onto a Surface that feeds a video encoder, modifying
 *       the output with a fragment shader.
 *   <li>Decode the second video and compare it to the expected result.
 * </ol><p>
 * The second step is a typical scenario for video editing.  We could do all this in one
 * step, feeding data through multiple stages of MediaCodec, but at some point we're
 * no longer exercising the code in the way we expect it to be used (and the code
 * gets a bit unwieldy).
 */
public class DecodeEditEncodeTest extends Thread {
    // where to put the output file (note: /sdcard requires WRITE_EXTERNAL_STORAGE permission)
    private static final File FILE_PATH = Environment.getExternalStorageDirectory();
    private static final String TAG = "DecodeEditEncode";
    private static final boolean WORK_AROUND_BUGS = false;  // avoid fatal codec bugs
    private static final boolean VERBOSE = true;           // lots of logging
    private static final boolean DEBUG_SAVE_FILE = false;   // save copy of encoded movie
    private static final String DEBUG_FILE_NAME_BASE = "/sdcard/test.";

    // parameters for the encoder
    private static final String MIME_TYPE = "video/avc";    // H.264 Advanced Video Coding
    private static final int FRAME_RATE = 60;               // 60fps
    private static final int IFRAME_INTERVAL = 10;          // 10 seconds between I-frames

    // movie length, in frames
    private static final int NUM_FRAMES = 30;               // two seconds of video

    private static final int TEST_R0 = 0;                   // dull green background
    private static final int TEST_G0 = 136;
    private static final int TEST_B0 = 0;
    private static final int TEST_R1 = 236;                 // pink; BT.601 YUV {120,160,200}
    private static final int TEST_G1 = 50;
    private static final int TEST_B1 = 186;


    private MediaExtractor mExtractor;
    MediaMuxer mMuxer;
    MediaCodec decoder;
    MediaCodec encoder;
    InputSurface inputSurface;
    OutputSurface outputSurface;
    private String filePaths[];
    boolean mMuxerStarted;


    public void init(String[] str) {
        filePaths = str;
    }

    @Override
    public void run() {
        try {
            testVideoEdit720p();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    // Replaces TextureRender.FRAGMENT_SHADER during edit; swaps green and blue channels.
    private static final String FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "precision mediump float;\n" +
                    "varying vec2 vTextureCoord;\n" +
                    "uniform samplerExternalOES sTexture;\n" +
                    "void main() {\n" +
                    "  gl_FragColor = texture2D(sTexture, vTextureCoord).rbga;\n" +
                    "}\n";

    // size of a frame, in pixels
    private int mWidth = -1;
    private int mHeight = -1;
    // bit rate, in bits per second
    private int mBitRate = -1;

    // largest color component delta seen (i.e. actual vs. expected)
    private int mLargestColorDelta;


    public void testVideoEditQCIF() throws Throwable {
        setParameters(176, 144, 1000000);
        VideoEditWrapper.runTest(this);
    }
    public void testVideoEditQVGA() throws Throwable {
        setParameters(320, 240, 2000000);
        VideoEditWrapper.runTest(this);
    }
    public void testVideoEdit720p() throws Throwable {
        setParameters(1920, 1920, 6000000);
        VideoEditWrapper.runTest(this);
    }

    /**
     * Wraps testEditVideo, running it in a new thread.  Required because of the way
     * SurfaceTexture.OnFrameAvailableListener works when the current thread has a Looper
     * configured.
     */
    private static class VideoEditWrapper implements Runnable {
        private Throwable mThrowable;
        private DecodeEditEncodeTest mTest;

        private VideoEditWrapper(DecodeEditEncodeTest test) {
            mTest = test;
        }

        @Override
        public void run() {
            try {
                mTest.videosEditTest();
            } catch (Throwable th) {
                mThrowable = th;
            }
        }

        /** Entry point. */
        public static void runTest(DecodeEditEncodeTest obj) throws Throwable {
            VideoEditWrapper wrapper = new VideoEditWrapper(obj);
            Thread th = new Thread(wrapper, "codec test");
            th.start();
            //th.join();
            if (wrapper.mThrowable != null) {
                throw wrapper.mThrowable;
            }
        }
    }

    /**
     * Sets the desired frame size and bit rate.
     */
    private void setParameters(int width, int height, int bitRate) {
        if ((width % 16) != 0 || (height % 16) != 0) {
            Log.w(TAG, "WARNING: width or height not multiple of 16");
        }
        mWidth = width;
        mHeight = height;
        mBitRate = bitRate;
    }

    /**
     * Tests editing of a video file with GL.
     */
    //original, is working successfully
    private void videoEditTest() {
        editVideoFile(filePaths[0]);
    }


    private void editVideoFile(String path) {
        if (VERBOSE) Log.d(TAG, "editVideoFile " + mWidth + "x" + mHeight);

        decoder = null;
        encoder = null;
        inputSurface = null;
        outputSurface = null;
        mMuxerStarted = false;

        try {
            // Create an encoder format that matches the input format.  (Might be able to just
            // re-use the format used to generate the video, since we want it to be the same.)
            MediaFormat outputFormat = MediaFormat.createVideoFormat(MIME_TYPE, mWidth, mHeight);
            outputFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                    MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
            outputFormat.setInteger(MediaFormat.KEY_BIT_RATE,mBitRate);
            outputFormat.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);
            outputFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL,5);


            mExtractor = new MediaExtractor();
            mExtractor.setDataSource(path);

            for (int i = 0; i < mExtractor.getTrackCount(); i++) {
                MediaFormat format = mExtractor.getTrackFormat(i);
                String mime = format.getString(MediaFormat.KEY_MIME);

                ///format change
                format.setInteger(MediaFormat.KEY_BIT_RATE,mBitRate);
                format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL,5);
                format.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);

                if (mime.startsWith("video/")) {
                    mExtractor.selectTrack(i);
                    try {
                        Log.d(TAG, "format : " + format);
                        encoder = MediaCodec.createEncoderByType(mime);
                        encoder.configure(outputFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
                        inputSurface = new InputSurface(encoder.createInputSurface());
                        inputSurface.makeCurrent();
                        encoder.start();
                        // OutputSurface uses the EGL context created by InputSurface.
                        ///decoder = MediaCodec.createDecoderByType(MIME_TYPE);
                        decoder = MediaCodec.createDecoderByType(mime);
                        outputSurface = new OutputSurface();
                        //outputSurface.changeFragmentShader(FRAGMENT_SHADER);
                        decoder.configure(format, outputSurface.getSurface(), null, 0);
                        decoder.start();
                    } catch (IllegalStateException e) {
                        Log.e(TAG, "codec '" + mime + "' failed configuration. " + e);
                    }

                }

            }

            editVideoData();
        }catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (VERBOSE) Log.d(TAG, "shutting down encoder, decoder");
            if (outputSurface != null) {
                outputSurface.release();
            }
            if (inputSurface != null) {
                inputSurface.release();
            }
            if (encoder != null) {
                encoder.stop();
                encoder.release();
            }
            if (decoder != null) {
                decoder.stop();
                decoder.release();
            }
            if(mMuxerStarted == true) {
                mMuxer.stop();
                mMuxer.release();
            }
        }
    }


    /**
     * Edits a stream of video data.
     */
    //!!

    private void editVideoData() {
        //!!
        // Output filename.  Ideally this would use Context.getFilesDir() rather than a
        // hard-coded output directory.

        try {
            String outputPath = new File(FILE_PATH,
                    "test_image.mp4").toString();
            Log.i(TAG, "Output file is " + outputPath);
            mMuxer = new MediaMuxer(outputPath, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
            Log.d(TAG, "mMuxer created");
        } catch (IOException e) {
            e.printStackTrace();
        }

        int mTrackIndex = -1;
        //!!
        final int TIMEOUT_USEC = 10000;
        ByteBuffer[] decoderInputBuffers = decoder.getInputBuffers();
        ByteBuffer[] encoderOutputBuffers = encoder.getOutputBuffers();
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        int inputChunk = 0;
        int outputCount = 0;

        boolean outputDone = false;
        boolean inputDone = false;
        boolean eosReceived = false;

        while (!outputDone) {
            if (VERBOSE) Log.d(TAG, "edit loop");

            // Assume output is available.  Loop until both assumptions are false.
            boolean decoderOutputAvailable = !eosReceived;
            boolean encoderOutputAvailable = true;
            while (decoderOutputAvailable || encoderOutputAvailable) {
                // Encoder is drained, check to see if we've got a new frame of output from
                // the decoder.  (The output is going to a Surface, rather than a ByteBuffer,
                // but we still get information through BufferInfo.)
                if (!eosReceived) {
                    int inputIndex = decoder.dequeueInputBuffer(10000);
                    if (inputIndex >= 0) {
                        // fill inputBuffers[inputBufferIndex] with valid data
///                ByteBuffer inputBuffer = inputBuffers[inputIndex];
                        ByteBuffer inputBuffer = decoder.getInputBuffer(inputIndex);
                        int sampleSize = mExtractor.readSampleData(inputBuffer, 0);
                        if (mExtractor.advance() && sampleSize > 0) {
                            decoder.queueInputBuffer(inputIndex, 0, sampleSize, mExtractor.getSampleTime(), 0);
                        } else {
                            Log.d(TAG, "InputBuffer BUFFER_FLAG_END_OF_STREAM");
                            decoder.queueInputBuffer(inputIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
///                    isInput = false;
                        }
                    }

                    //copy below
                    int decoderStatus = decoder.dequeueOutputBuffer(info, TIMEOUT_USEC);

                    switch(decoderStatus) {
                        case MediaCodec.INFO_TRY_AGAIN_LATER:
                            // no output available yet
                            if (VERBOSE) Log.d(TAG, "no output from decoder available");
                            decoderOutputAvailable = false;
                            break;
                        case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                            //decoderOutputBuffers = decoder.getOutputBuffers();
                            if (VERBOSE) Log.d(TAG, "decoder output buffers changed (we don't care)");
                            break;
                        case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                            // expected before first buffer of data
                            MediaFormat newFormat = decoder.getOutputFormat();
                            if (VERBOSE) Log.d(TAG, "decoder output format changed: " + newFormat);
                            break;
                        default: // decoderStatus >= 0
                            if (decoderStatus < 0) {
                                fail("unexpected result from decoder.dequeueOutputBuffer: "+decoderStatus);
                            }
                            else {
                                if (VERBOSE) Log.d(TAG, "surface decoder given buffer "
                                        + decoderStatus + " (size=" + info.size + ")");
                                // The ByteBuffers are null references, but we still get a nonzero
                                // size for the decoded data.
                                boolean doRender = (info.size != 0);

                                // As soon as we call releaseOutputBuffer, the buffer will be forwarded
                                // to SurfaceTexture to convert to a texture.  The API doesn't
                                // guarantee that the texture will be available before the call
                                // returns, so we need to wait for the onFrameAvailable callback to
                                // fire.  If we don't wait, we risk rendering from the previous frame.
                                decoder.releaseOutputBuffer(decoderStatus, doRender);
                                if (doRender) {
                                    // This waits for the image and renders it after it arrives.
                                    if (VERBOSE) Log.d(TAG, "awaiting frame");
                                    outputSurface.awaitNewImage();
                                    outputSurface.drawImage();
                                    //outputSurface.drawBitmap();

                                    // Send it to the encoder.
                                    inputSurface.setPresentationTime(info.presentationTimeUs * 1000);
                                    if (VERBOSE) Log.d(TAG, "swapBuffers");
                                    inputSurface.swapBuffers();
                                }
                                if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                                    // forward decoder EOS to encoder
                                    if (VERBOSE) Log.d(TAG, "signaling input EOS");
                                    if (WORK_AROUND_BUGS) {
                                        // Bail early, possibly dropping a frame.
                                        return;
                                    } else {
                                        decoderOutputAvailable = false;
                                        eosReceived = true;
                                        encoder.signalEndOfInputStream();
                                    }
                                }

                                //erase if there is an error
                                if ((info.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                                    info.size = 0;
                                }
                            }
                            break;
                    }
                }


                // Start by draining any pending output from the encoder.  It's important to
                // do this before we try to stuff any more data in.
                int encoderStatus = encoder.dequeueOutputBuffer(info, TIMEOUT_USEC);

                switch (encoderStatus) {
                    case MediaCodec.INFO_TRY_AGAIN_LATER:
                        // no output available yet
                        if (VERBOSE) Log.d(TAG, "no output from encoder available");
                        encoderOutputAvailable = false;
                        break;
                    case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                        encoderOutputBuffers = encoder.getOutputBuffers();
                        if (VERBOSE) Log.d(TAG, "encoder output buffers changed");
                        break;
                    case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                        MediaFormat newFormat = encoder.getOutputFormat();
                        if (VERBOSE) Log.d(TAG, "encoder output format changed: " + newFormat);
                        //!!
                        mTrackIndex = mMuxer.addTrack(newFormat);
                        mMuxer.start();
                        mMuxerStarted = true;
                        //!!
                        break;
                    default: // encoderStatus >= 0
                        if (encoderStatus < 0) {
                            fail("unexpected result from encoder.dequeueOutputBuffer: " + encoderStatus);
                        }
                        else {
                            ByteBuffer encodedData = encoderOutputBuffers[encoderStatus];
                            if (encodedData == null) {
                                fail("encoderOutputBuffer " + encoderStatus + " was null");
                            }

                            // Write the data to the output "file".
                            if (info.size != 0) {
                                encodedData.position(info.offset);
                                encodedData.limit(info.offset + info.size);

                                //!!
                                mMuxer.writeSampleData(mTrackIndex, encodedData, info);
                                //!!
                                if (VERBOSE) Log.d(TAG, "encoder output " + info.size + " bytes");
                            }
                            outputDone = (info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0;
                            encoder.releaseOutputBuffer(encoderStatus, false);
                        }
                        break;
                }
                if (encoderStatus != MediaCodec.INFO_TRY_AGAIN_LATER) {
                    // Continue attempts to drain output.
                    continue;
                }

                //////here
                /*
                if(!eosReceived) {
                Log.d(TAG,"copy here");
                }
                */
            }
        }
    }


    //for encoding videos to one file
    private void videosEditTest() {
        editVideoFiles();
    }

    private void editVideoFiles() {
        if (VERBOSE) Log.d(TAG, "editVideoFiles " + mWidth + "x" + mHeight);

        decoder = null;
        encoder = null;
        inputSurface = null;
        outputSurface = null;
        mMuxerStarted = false;

        try {
            String outputPath = new File(FILE_PATH,
                    "many_video.mp4").toString();
            Log.i(TAG, "Output file is " + outputPath);
            mMuxer = new MediaMuxer(outputPath, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
            Log.d(TAG, "mMuxer created");
        } catch (IOException e) {
            e.printStackTrace();
        }
        editVideoDatas();

        if (VERBOSE) Log.d(TAG, "shutting down encoder, decoder");
        if (outputSurface != null) {
            outputSurface.release();
        }
        if (inputSurface != null) {
            inputSurface.release();
        }
        if (encoder != null) {
            encoder.stop();
            encoder.release();
        }
        if (decoder != null) {
            decoder.stop();
            decoder.release();
        }
        if(mMuxerStarted == true) {
            mMuxer.stop();
            mMuxer.release();
        }
    }

    public void initMediaCodec(String path, boolean isFirstTime) {
        try {

            mExtractor = new MediaExtractor();
            mExtractor.setDataSource(path);

            for (int i = 0; i < mExtractor.getTrackCount(); i++) {
                MediaFormat format = mExtractor.getTrackFormat(i);
                String mime = format.getString(MediaFormat.KEY_MIME);
                if (mime.startsWith("video/")) {
                    mExtractor.selectTrack(i);
                    try {
                        Log.d(TAG, "format : " + format);
                        if(isFirstTime) {
                            // Create an encoder format that matches the input format.  (Might be able to just
                            // re-use the format used to generate the video, since we want it to be the same.)
                            MediaFormat outputFormat = MediaFormat.createVideoFormat(MIME_TYPE, mWidth, mHeight);
                            outputFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                                    MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
                            outputFormat.setInteger(MediaFormat.KEY_BIT_RATE,mBitRate);
                            outputFormat.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);
                            outputFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL,5);

                            encoder = MediaCodec.createEncoderByType(mime);
                            encoder.configure(outputFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
                            inputSurface = new InputSurface(encoder.createInputSurface());
                            inputSurface.makeCurrent();
                            encoder.start();
                            // OutputSurface uses the EGL context created by InputSurface.
                            ///decoder = MediaCodec.createDecoderByType(MIME_TYPE);
                            outputSurface = new OutputSurface();
                        }
                        decoder = MediaCodec.createDecoderByType(mime);
                        //outputSurface.changeFragmentShader(FRAGMENT_SHADER);
                        decoder.configure(format, outputSurface.getSurface(), null, 0);
                        decoder.start();
                    } catch (IllegalStateException e) {
                        Log.e(TAG, "codec '" + mime + "' failed configuration. " + e);
                    }

                }

            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void editVideoDatas() {
        Log.d(TAG, "start editVideoDatas");
        boolean outputDone = false;
        boolean eosReceived = false;
        int fileInd = 0;
        int mTrackIndex = -1;
        boolean isFirstTime = true;

        long presentationTimeUsLast = 0;

        initMediaCodec(filePaths[fileInd],true);
        fileInd++;

        final int TIMEOUT_USEC = 10000;
        ByteBuffer[] encoderOutputBuffers = encoder.getOutputBuffers();
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();


        Log.d(TAG,"outputDone:"+Boolean.toString(outputDone));

        while (!outputDone) {
            if (VERBOSE) Log.d(TAG, "edit loop");

            Log.d(TAG, "fileIndex="+fileInd+", eosReceived="+Boolean.toString(eosReceived));
            if(fileInd < filePaths.length && eosReceived == true) {
                Log.d(TAG, "try to init codec #" + fileInd);
                initMediaCodec(filePaths[fileInd], false);
                fileInd++;
                eosReceived = false;
            }
            // Assume output is available.  Loop until both assumptions are false.
            boolean decoderOutputAvailable = !eosReceived;
            boolean encoderOutputAvailable = true;
            while (decoderOutputAvailable || encoderOutputAvailable) {
                // Encoder is drained, check to see if we've got a new frame of output from
                // the decoder.  (The output is going to a Surface, rather than a ByteBuffer,
                // but we still get information through BufferInfo.)
                if (!eosReceived) {
                    int inputIndex = decoder.dequeueInputBuffer(10000);
                    if (inputIndex >= 0) {
                        // fill inputBuffers[inputBufferIndex] with valid data
                        ByteBuffer inputBuffer = decoder.getInputBuffer(inputIndex);
                        int sampleSize = mExtractor.readSampleData(inputBuffer, 0);
                        if (mExtractor.advance() && sampleSize > 0) {
                            decoder.queueInputBuffer(inputIndex, 0, sampleSize, mExtractor.getSampleTime(), 0);
                        } else {
                            Log.d(TAG, "InputBuffer BUFFER_FLAG_END_OF_STREAM");
                            decoder.queueInputBuffer(inputIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                        }
                    }

                    //copy below
                    int decoderStatus = decoder.dequeueOutputBuffer(info, TIMEOUT_USEC);

                    switch(decoderStatus) {
                        case MediaCodec.INFO_TRY_AGAIN_LATER:
                            // no output available yet
                            if (VERBOSE) Log.d(TAG, "no output from decoder available");
                            decoderOutputAvailable = false;
                            break;
                        case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                            if (VERBOSE) Log.d(TAG, "decoder output buffers changed (we don't care)");
                            break;
                        case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                            // expected before first buffer of data
                            MediaFormat newFormat = decoder.getOutputFormat();
                            if (VERBOSE) Log.d(TAG, "decoder output format changed: " + newFormat);
                            break;
                        default: // decoderStatus >= 0
                            if (decoderStatus < 0) {
                                fail("unexpected result from decoder.dequeueOutputBuffer: "+decoderStatus);
                            }
                            else {
                                if (VERBOSE) Log.d(TAG, "surface decoder given buffer "
                                        + decoderStatus + " (size=" + info.size + ")");
                                // The ByteBuffers are null references, but we still get a nonzero
                                // size for the decoded data.
                                boolean doRender = (info.size != 0);

                                // As soon as we call releaseOutputBuffer, the buffer will be forwarded
                                // to SurfaceTexture to convert to a texture.  The API doesn't
                                // guarantee that the texture will be available before the call
                                // returns, so we need to wait for the onFrameAvailable callback to
                                // fire.  If we don't wait, we risk rendering from the previous frame.
                                decoder.releaseOutputBuffer(decoderStatus, doRender);
                                if (doRender) {
                                    // This waits for the image and renders it after it arrives.
                                    if (VERBOSE) Log.d(TAG, "awaiting frame");
                                    outputSurface.awaitNewImage();
                                    outputSurface.drawImage();
                                    //outputSurface.drawBitmap();

                                    info.presentationTimeUs += presentationTimeUsLast;
                                    // Send it to the encoder.
                                    inputSurface.setPresentationTime((info.presentationTimeUs) * 1000);
                                    if (VERBOSE) Log.d(TAG, "swapBuffers");
                                    inputSurface.swapBuffers();
                                }
                                if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                                    // forward decoder EOS to encoder
                                    Log.d(TAG,"fileIndex = "+fileInd+" , length = " + filePaths.length);

                                    if (WORK_AROUND_BUGS) {
                                        // Bail early, possibly dropping a frame.
                                        return;
                                    } else {
                                        decoderOutputAvailable = false;
                                        eosReceived = true;
                                        presentationTimeUsLast += info.presentationTimeUs;

                                        Log.d(TAG,"eosReceived, presentationTimeUsLast; " + presentationTimeUsLast);
                                        if(fileInd == filePaths.length) {
                                            if (VERBOSE) Log.d(TAG, "signaling input EOS");
                                            encoder.signalEndOfInputStream();
                                        }
                                        else {
                                            decoder.stop();
                                            decoder.release();
                                            mExtractor.release();
                                        }
                                    }
                                }

                                //erase if there is an error
                                if ((info.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                                    info.size = 0;
                                }
                            }
                            break;
                    }
                }

                // Start by draining any pending output from the encoder.  It's important to
                // do this before we try to stuff any more data in.
                int encoderStatus = encoder.dequeueOutputBuffer(info, TIMEOUT_USEC);

                switch (encoderStatus) {
                    case MediaCodec.INFO_TRY_AGAIN_LATER:
                        // no output available yet
                        if (VERBOSE) Log.d(TAG, "no output from encoder available");
                        encoderOutputAvailable = false;
                        break;
                    case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                        encoderOutputBuffers = encoder.getOutputBuffers();
                        if (VERBOSE) Log.d(TAG, "encoder output buffers changed");
                        break;
                    case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                        MediaFormat newFormat = encoder.getOutputFormat();
                        if (VERBOSE) Log.d(TAG, "encoder output format changed: " + newFormat);
                        //!!
                        mTrackIndex = mMuxer.addTrack(newFormat);
                        mMuxer.start();
                        mMuxerStarted = true;
                        //!!
                        break;
                    default: // encoderStatus >= 0
                        if (encoderStatus < 0) {
                            fail("unexpected result from encoder.dequeueOutputBuffer: " + encoderStatus);
                        }
                        else {
                            ByteBuffer encodedData = encoderOutputBuffers[encoderStatus];
                            if (encodedData == null) {
                                fail("encoderOutputBuffer " + encoderStatus + " was null");
                            }

                            // Write the data to the output "file".
                            if (info.size != 0) {
                                encodedData.position(info.offset);
                                encodedData.limit(info.offset + info.size);

                                //!!
                                mMuxer.writeSampleData(mTrackIndex, encodedData, info);
                                //!!
                                if (VERBOSE) Log.d(TAG, "encoder output " + info.size + " bytes");
                            }
                            outputDone = (info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0;
                            encoder.releaseOutputBuffer(encoderStatus, false);
                        }
                        break;
                }
                if (encoderStatus != MediaCodec.INFO_TRY_AGAIN_LATER) {
                    // Continue attempts to drain output.
                    continue;
                }
            }
        }
    }
}

