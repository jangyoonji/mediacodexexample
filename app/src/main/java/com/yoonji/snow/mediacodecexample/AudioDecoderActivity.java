package com.yoonji.snow.mediacodecexample;

/**
 * Created by snow on 2017-08-07.
 */


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class AudioDecoderActivity extends Activity {

    protected static AudioDecoderThread mAudioDecoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_decoder);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAudioDecoder._stop();
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private static final String[] SAMPLE = {"/storage/emulated/0/Movies/looks/LOOKS_20161115_125437.mp4"};

        public PlaceholderFragment() {
            mAudioDecoder = new AudioDecoderThread();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_audio_decoder, container, false);

            final Button btn = (Button) rootView.findViewById(R.id.play);
            btn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    mAudioDecoder.init(SAMPLE);
                    mAudioDecoder.start();
                }
            });
            return rootView;
        }
    }
}