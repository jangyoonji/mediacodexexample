package com.yoonji.snow.mediacodecexample;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.File;

@RequiresApi(api = Build.VERSION_CODES.M)
public class ImageEncodingActivity extends AppCompatActivity {
    private static final String TAG = "ImageEncoderActivity";
    private final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 100;
    // where to put the output file (note: /sdcard requires WRITE_EXTERNAL_STORAGE permission)
    private static final File OUTPUT_DIR = Environment.getExternalStorageDirectory();
    private ImageEncodingThread imageEncodingThread;

    private static final String[] FILE_PATH =
            {
                    "/storage/emulated/0/DCIM/Camera/20170727_201914.mp4"
            };
    //private static final String input_path = "/storage/emulated/0/DCIM/Camera/20170802_204648.jpg";
    private static final String input_path = "/storage/emulated/0/Foodie/2017-08-06-17-12-59.jpg";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }


        imageEncodingThread = new ImageEncodingThread();
        try {
            imageEncodingThread.testVideoEdit720p(input_path);
        } catch (Throwable th) {th.printStackTrace(); }

    }

    @Override
    protected  void onDestroy() {
        super.onDestroy();
        if(imageEncodingThread != null)
            imageEncodingThread = null;
    }
}
