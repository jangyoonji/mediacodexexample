package com.yoonji.snow.mediacodecexample;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.util.Log;
import android.view.Surface;
import android.widget.Toast;

import java.io.IOException;
import java.nio.ByteBuffer;

import static junit.framework.Assert.assertTrue;

/**
 * Created by snow on 2017-07-26.
 */

public class VideoDecoderThread extends Thread {
    private static final String VIDEO = "video/";
    private static final String AUDIO = "audio/";

    private static final String TAG = "VideoDecoder";
    private MediaExtractor mExtractor;
    private MediaCodec mDecoder;
    private boolean eosReceived;
    private Surface mSurface;

    private String[] filePaths;
    private MediaExtractor audioExtractor;
    private int fileInd;
    private int inputFileSize;
    private boolean isFirstFileIndex;


    public boolean init(Surface surface, String[] filePath) {
        eosReceived = false;
        mSurface = surface;
        filePaths = filePath;
        fileInd=0;
        inputFileSize = filePath.length;
        isFirstFileIndex = true;


        if(InitDecodingIndex(isFirstFileIndex) < 0)
            return false;
        /*
        try {
            mExtractor = new MediaExtractor();
            mExtractor.setDataSource(filePath[0]);

            audioExtractor = new MediaExtractor();
            audioExtractor.setDataSource(filePath[0]);

            int channel = 0;
            for (int i = 0; i < mExtractor.getTrackCount(); i++) {
                MediaFormat format = mExtractor.getTrackFormat(i);
                String mime = format.getString(MediaFormat.KEY_MIME);
                if (mime.startsWith(VIDEO)) {
                    mExtractor.selectTrack(i);
                    mDecoder = MediaCodec.createDecoderByType(mime);
                    try {
                        Log.d(TAG, "format : " + format);
                        mDecoder.configure(format, mSurface, null, 0); //0:decoder
                    } catch (IllegalStateException e) {
                        Log.e(TAG, "codec '" + mime + "' failed configuration. " + e);
                        return false;
                    }
                    mDecoder.start();
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        */
        isFirstFileIndex = false;
        return true;
    }

    @Override
    public void run() {
        /*
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
///        ByteBuffer[] inputBuffers = mDecoder.getInputBuffers();
///        mDecoder.getOutputBuffers();
        boolean isInput = true;
        boolean first = false;
        */
        while (InitDecodingIndex(isFirstFileIndex) < inputFileSize ) {
            MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
            boolean first = false;
            long startWhen = 0;
            while (!eosReceived) {

                int inputIndex = mDecoder.dequeueInputBuffer(10000);
                if (inputIndex >= 0) {
                    ByteBuffer inputBuffer = mDecoder.getInputBuffer(inputIndex);
                    int sampleSize = mExtractor.readSampleData(inputBuffer, 0);
                    if (mExtractor.advance() && sampleSize > 0) {
                        mDecoder.queueInputBuffer(inputIndex, 0, sampleSize, mExtractor.getSampleTime(), 0);
                    } else {
                        Log.d(TAG, "InputBuffer BUFFER_FLAG_END_OF_STREAM");
                        mDecoder.queueInputBuffer(inputIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                    }
                }
                int outIndex = mDecoder.dequeueOutputBuffer(info, 10000);
                if (outIndex >= 0) {
                    //not use here
                    ByteBuffer outputBuffer = mDecoder.getOutputBuffer(outIndex);
                    MediaFormat bufferFormat = mDecoder.getOutputFormat(outIndex);
                    //
                    switch (outIndex) {
                        case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                            Log.d(TAG, "INFO_OUTPUT_FORMAT_CHANGED format : " + mDecoder.getOutputFormat());
                            bufferFormat = mDecoder.getOutputFormat(outIndex);
                            break;
                        case MediaCodec.INFO_TRY_AGAIN_LATER:
//				Log.d(TAG, "INFO_TRY_AGAIN_LATER");

                            break;
                        case MediaCodec.BUFFER_FLAG_END_OF_STREAM:
                            //eosReceived=true;
                            Log.d(TAG, "End of Stream");
                        default:
                            if (!first) {
                                startWhen = System.currentTimeMillis();
                                first = true;
                            }
                        try {
                            long sleepTime = (info.presentationTimeUs / 1000) - (System.currentTimeMillis() - startWhen);
                            Log.d(TAG, "info.presentationTimeUs : " + (info.presentationTimeUs / 1000) + " playTime: " + (System.currentTimeMillis() - startWhen) + " sleepTime : " + sleepTime);
                            if (sleepTime > 0)
                                Thread.sleep(sleepTime);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                            mDecoder.releaseOutputBuffer(outIndex, true /* Surface init */);
                            break;
                    }
                }
                // All decoded frames have been rendered, we can stop playing now
                if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    Log.d(TAG, "OutputBuffer BUFFER_FLAG_END_OF_STREAM");
                    eosReceived = true;
                    break;
                }
            }
            mDecoder.stop();
            mDecoder.release();
            mExtractor.release();
            eosReceived=false;
            Log.d(TAG, "releasing decoder, extractor #"+fileInd);
            fileInd++;
            if(mSurface.isValid() == false) {
                break;
            }
        }
    }

    public int InitDecodingIndex(boolean isFirstFile) {
        if(isFirstFile)
            return fileInd;
        if(fileInd < inputFileSize) {
            try {
                Log.d(TAG, "trying to init decoder for file index #"+fileInd);
                mExtractor = new MediaExtractor();
                mExtractor.setDataSource(filePaths[fileInd]);

                audioExtractor = new MediaExtractor();
                audioExtractor.setDataSource(filePaths[fileInd]);

                int channel = 0;
                for (int i = 0; i < mExtractor.getTrackCount(); i++) {
                    MediaFormat format = mExtractor.getTrackFormat(i);
                    String mime = format.getString(MediaFormat.KEY_MIME);
                    if (mime.startsWith(VIDEO)) {
                        mExtractor.selectTrack(i);
                        mDecoder = MediaCodec.createDecoderByType(mime);
                        try {
                            Log.d(TAG, "format : " + format);
                            mDecoder.configure(format, mSurface, null, 0); //0:decoder
                        } catch (IllegalStateException e) {
                            Log.e(TAG, "codec '" + mime + "' failed configuration. " + e);
                            return -1;
                        }
                        mDecoder.start();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileInd;
    }

    public Surface getSurface() {
        return mSurface;
    }

    public void close() {
        eosReceived = true;
    }
}
