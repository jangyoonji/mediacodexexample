package com.yoonji.snow.mediacodecexample;

import android.graphics.SurfaceTexture;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.util.Log;
import android.view.Surface;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import static junit.framework.Assert.fail;

/**
 * Created by snow on 2017-07-28.
 */

public class DecodeEncodeThread extends Thread {
    private static final String VIDEO = "video/";
    private static final String AUDIO = "audio/";
    private static final String MIME_TYPE = "video/avc";

    private static final String TAG = "DecodeEncode Thread";
    private MediaExtractor mExtractor;
    private MediaCodec mDecoder;
    private boolean eosReceived;
    private Surface mSurface;

    private String[] filePaths;
    private MediaExtractor audioExtractor;
    private int fileInd;
    private int inputFileSize;
    private boolean isFirstFileIndex;

    //!
    private boolean VERBOSE = true;
    private boolean WORK_AROUND_BUGS = true;
    private MediaCodec mEncoder;
    private InputSurface mInputSurface;
    private OutputSurface mOutputSurface;
    MediaMuxer mMuxer;
    File output_dir;
    int mTrackIndex;
    MediaFormat outputFormat;
    MediaFormat inputFormat;
    private int encWidth = 640;
    private int encHeight = 480;
    private int encBitRate = 6000000;      // Mbps
    private boolean ismMuxerStarted;

    // Replaces TextureRender.FRAGMENT_SHADER during edit;
    private static final String FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "precision mediump float;\n" +
                    "varying vec2 vTextureCoord;\n" +
                    "uniform samplerExternalOES sTexture;\n" +
                    "void main() {\n" +
                    "  gl_FragColor = texture2D(sTexture, vTextureCoord).rgba;\n" +
                    "}\n";

    public boolean init(String[] filePath, File outputDir) {
        eosReceived = false;
        filePaths = filePath;
        fileInd=0;
        inputFileSize = filePath.length;
        isFirstFileIndex = true;

        mInputSurface = null;
        mOutputSurface = null;
        mDecoder = null;
        mEncoder = null;
        mMuxer = null;
        if(InitDecodingIndex(isFirstFileIndex) < 0)
            return false;

        //!
        output_dir = outputDir;
        encWidth = 640;
        encHeight = 480;
        encBitRate = 6000000;      // Mbps
        ismMuxerStarted = false;

        mTrackIndex = -1;



        isFirstFileIndex = false;
        return true;
    }

    public void testVideoEditQCIF() throws Throwable {
        setParameters(176, 144, 1000000);
        DecodeEncodeThread.VideoEditWrapper.runTest(this);
    }
    private void setParameters(int width, int height, int bitRate) {
        if ((width % 16) != 0 || (height % 16) != 0) {
            Log.w(TAG, "WARNING: width or height not multiple of 16");
        }
        encWidth = width;
        encHeight = height;
        encBitRate = bitRate;
    }
    /**
     * Wraps testEditVideo, running it in a new thread.  Required because of the way
     * SurfaceTexture.OnFrameAvailableListener works when the current thread has a Looper
     * configured.
     */
    private static class VideoEditWrapper implements Runnable {
        private Throwable mThrowable;
        private DecodeEncodeThread mTest;

        private VideoEditWrapper(DecodeEncodeThread test) {
            mTest = test;
        }

        @Override
        public void run() {
            try {
                mTest.videoEditTest();
            } catch (Throwable th) {
                mThrowable = th;
            }
        }

        /** Entry point. */
        public static void runTest(DecodeEncodeThread obj) throws Throwable {
            DecodeEncodeThread.VideoEditWrapper wrapper = new DecodeEncodeThread.VideoEditWrapper(obj);
            Thread th = new Thread(wrapper, "codec test");
            th.start();
            //th.join();
            if (wrapper.mThrowable != null) {
                throw wrapper.mThrowable;
            }
        }
    }

    private void videoEditTest() {

        editVideoFile();
    }

    @Override
    public void run() {
        while (InitDecodingIndex(isFirstFileIndex) <= inputFileSize ) {
            MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
            boolean isInput = true;
            boolean first = false;
            long startWhen = 0;
            final int TIMEOUT_USEC = 10000;
            while (!eosReceived) {
                int inputIndex = mDecoder.dequeueInputBuffer(10000);
                if (inputIndex >= 0) {
                    // fill inputBuffers[inputBufferIndex] with valid data
                    ByteBuffer inputBuffer = mDecoder.getInputBuffer(inputIndex);
                    int sampleSize = mExtractor.readSampleData(inputBuffer, 0);
                    if (mExtractor.advance() && sampleSize > 0) {
                        Log.d(TAG, "decoder queueInputBuffer");
                        mDecoder.queueInputBuffer(inputIndex, 0, sampleSize, mExtractor.getSampleTime(), 0);
                    } else {
                        Log.d(TAG, "InputBuffer BUFFER_FLAG_END_OF_STREAM");
                        mDecoder.queueInputBuffer(inputIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                    }
                }

                int outIndex = mDecoder.dequeueOutputBuffer(info, 10000);
                if (outIndex >= 0) {
                    switch (outIndex) {
                        case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                            Log.d(TAG, "INFO_OUTPUT_FORMAT_CHANGED format : " + mDecoder.getOutputFormat());
                            break;
                        case MediaCodec.INFO_TRY_AGAIN_LATER:
                            Log.d(TAG, "decoder try again later");
                            break;
                        case MediaCodec.BUFFER_FLAG_END_OF_STREAM:
                            //eosReceived=true;
                            Log.d(TAG, "End of Stream");
                            break;
                        default:
                            if (!first) {
                                startWhen = System.currentTimeMillis();
                                first = true;
                            }

                            /*
                            try {
                                long sleepTime = (info.presentationTimeUs / 1000) - (System.currentTimeMillis() - startWhen);
                                Log.d(TAG, "info.presentationTimeUs : " + (info.presentationTimeUs / 1000) + " playTime: " + (System.currentTimeMillis() - startWhen) + " sleepTime : " + sleepTime);
                                if (sleepTime > 0)
                                    Thread.sleep(sleepTime);
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            */
                            mDecoder.releaseOutputBuffer(outIndex, false /* Surface init */);
                            //mInputSurface.setPresentationTime(computePresentationTime(outIndex) * 1000);
                            mInputSurface.setPresentationTime(mOutputSurface.getTimeStamp());
                            if (VERBOSE) Log.d(TAG, "inputSurface swapBuffers");
                            mInputSurface.swapBuffers();
                            break;
                    }
                }

                // All decoded frames have been rendered, we can stop playing now
                if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    Log.d(TAG, "OutputBuffer BUFFER_FLAG_END_OF_STREAM");
                    eosReceived = true;
                    //break;
                }

                // Check for output from the encoder.  If there's no output yet, we either need to
                // provide more input, or we need to wait for the encoder to work its magic.  We
                // can't actually tell which is the case, so if we can't get an output buffer right
                // away we loop around and see if it wants more input.
                //
                // If we do find output, drain it all before supplying more input.
                ByteBuffer[] encoderOutputBuffers = mEncoder.getOutputBuffers();
                while (true) {
                    Log.d(TAG,"encoding while loop");
                    int encoderStatus = mEncoder.dequeueOutputBuffer(info, TIMEOUT_USEC);
                    if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                        // no output available yet
                        if (VERBOSE) Log.d(TAG, "no output from encoder available in run");
                        break;      // out of while
                    } else if (encoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                        // not expected for an encoder
                        encoderOutputBuffers = mEncoder.getOutputBuffers();
                        if (VERBOSE) Log.d(TAG, "encoder output buffers changed");
                    } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                        // not expected for an encoder
                        if(ismMuxerStarted)
                            Log.d(TAG,"error: format changed twice");
                        MediaFormat newFormat = mEncoder.getOutputFormat();
                        if (VERBOSE) Log.d(TAG, "encoder output format changed: " + newFormat);

                        //now that we have the Magic Goodies, start the muxer
                        mTrackIndex = mMuxer.addTrack(newFormat);
                        mMuxer.start();
                        ismMuxerStarted = true;
                        Log.d(TAG,"muxer start");
                    } else if (encoderStatus < 0) {
                        Log.d(TAG,"unexpected result from encoder.dequeueOutputBuffer: " + encoderStatus);
                    } else { // encoderStatus >= 0
                        //ByteBuffer encodedData = mEncoder.getOutputBuffer(encoderStatus);
                        ByteBuffer encodedData = encoderOutputBuffers[encoderStatus];
                        if (encodedData == null) {
                            Log.d(TAG,"encoderOutputBuffer " + encoderStatus + " was null");
                        }

                        // Codec config flag must be set iff this is the first chunk of output.  This
                        // may not hold for all codecs, but it appears to be the case for video/avc.
                        //  if((info.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0 ||
                        //          outputCount != 0);

                        if (info.size != 0) {
                            if (!ismMuxerStarted) {
                                throw new RuntimeException("muxer hasn't started");
                            }
                            // Adjust the ByteBuffer values to match BufferInfo.
                            encodedData.position(info.offset);
                            encodedData.limit(info.offset + info.size);

                            mMuxer.writeSampleData(mTrackIndex, encodedData, info);
                            if (VERBOSE) Log.d(TAG, "sent " + info.size + " bytes to muxer");
                        }

                        mEncoder.releaseOutputBuffer(encoderStatus, false);
                        Log.d(TAG, "encoder releaseOutputBuffer");
                        if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                            break;      // out of while
                        }
                    }
                }
            }



            mDecoder.stop();
            mDecoder.release();
            mExtractor.release();
            eosReceived=false;
            Log.d(TAG, "releasing decoder, extractor #"+fileInd);
        }
        //!!
        if (VERBOSE) Log.d(TAG, "signaling input EOS");
        mEncoder.signalEndOfInputStream();
        releaseEncoder();

    }



    public void editVideoFile() {
        if (VERBOSE) Log.d(TAG, "editVideoFile " + encWidth + "x" + encHeight);

        MediaFormat format = MediaFormat.createVideoFormat(MIME_TYPE, encWidth, encHeight);

        String inputData = filePaths[0];
        try {

            // Create an encoder format that matches the input format.  (Might be able to just
            // re-use the format used to generate the video, since we want it to be the same.)
            MediaFormat outputFormat = MediaFormat.createVideoFormat(MIME_TYPE, encWidth, encHeight);
            outputFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                    MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
            outputFormat.setInteger(MediaFormat.KEY_BIT_RATE, 125000);
            outputFormat.setInteger(MediaFormat.KEY_FRAME_RATE, 30);
            outputFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL,5);


            mEncoder = MediaCodec.createEncoderByType(MIME_TYPE);
            mEncoder.configure(outputFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            mInputSurface = new InputSurface(mEncoder.createInputSurface());
            mInputSurface.makeCurrent();
            mEncoder.start();

            //initialize
            if (VERBOSE) Log.d(TAG, "preparing for decoder");
            mExtractor = new MediaExtractor();
            try {
                mExtractor.setDataSource(inputData);
                for (int i = 0; i < mExtractor.getTrackCount(); i++) {
                    inputFormat = mExtractor.getTrackFormat(i);
                    String mime = inputFormat.getString(MediaFormat.KEY_MIME);
                    if (mime.startsWith(VIDEO)) {
                        //!
                        mExtractor.selectTrack(i);
                        mDecoder = MediaCodec.createDecoderByType(mime);
                        mOutputSurface = new OutputSurface();
                        mOutputSurface.changeFragmentShader(FRAGMENT_SHADER);
                        try {
                            Log.d(TAG, "format : " + inputFormat);
                            mDecoder.configure(inputFormat, mOutputSurface.getSurface(), null, 0);
                        } catch (IllegalStateException e) {
                            Log.e(TAG, "codec '" + mime + "' failed configuration. " + e);
                            return;
                        }
                        mDecoder.start();
                    }
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
            // OutputSurface uses the EGL context created by InputSurface.
            editVideoData();

        } catch ( IOException e) { e.printStackTrace(); }
        finally {

            if (VERBOSE) Log.d(TAG, "shutting down encoder, decoder");
            if (mOutputSurface != null) {
                mOutputSurface.release();
            }
            if (mInputSurface != null) {
                mInputSurface.release();
            }
            if (mEncoder != null) {
                mEncoder.stop();
                mEncoder.release();
            }
            if (mDecoder != null) {
                mDecoder.stop();
                mDecoder.release();
            }

            if(ismMuxerStarted == true) {
                mMuxer.stop();
                mMuxer.release();
            }
        }
    }

    /**
     * Edits a stream of video data.
     */
    private void editVideoData() {
        try {
            String outputPath = new File(output_dir,
                    "test2.mp4").toString();
            Log.i(TAG, "Output file is " + outputPath);
            mMuxer = new MediaMuxer(outputPath, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
            Log.d(TAG, "mMuxer created");
        } catch (IOException e) {
            e.printStackTrace();
        }
        mTrackIndex = -1;

        final int TIMEOUT_USEC = 10000;
        ByteBuffer[] decoderInputBuffers = mDecoder.getInputBuffers();
        ByteBuffer[] encoderOutputBuffers = mEncoder.getOutputBuffers();
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        int inputChunk = 0;
        int outputCount = 0;

        boolean outputDone = false;
        boolean inputDone = false;
        //boolean decoderDone = false;
        eosReceived = false;

        while (!outputDone) {
            if (VERBOSE) Log.d(TAG, "edit loop");

            // Feed more data to the decoder.
            if (!eosReceived) {
                int inputBufIndex = mDecoder.dequeueInputBuffer(TIMEOUT_USEC);
                if (inputBufIndex >= 0) {
                    // fill inputBuffers[inputBufferIndex] with valid data
///                ByteBuffer inputBuffer = inputBuffers[inputIndex];
                    ByteBuffer inputBuffer = mDecoder.getInputBuffer(inputBufIndex);
                    int sampleSize = mExtractor.readSampleData(inputBuffer, 0);
                    if (mExtractor.advance() && sampleSize > 0) {
                        mDecoder.queueInputBuffer(inputBufIndex, 0, sampleSize, mExtractor.getSampleTime(), 0);
                    } else {
                        Log.d(TAG, "InputBuffer BUFFER_FLAG_END_OF_STREAM");
                        mDecoder.queueInputBuffer(inputBufIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
///                    isInput = false;
                    }
                } else {
                    if (VERBOSE) Log.d(TAG, "input buffer not available");
                }
            }


            // Assume output is available.  Loop until both assumptions are false.
            boolean decoderOutputAvailable = !eosReceived;
            boolean encoderOutputAvailable = true;
            while (decoderOutputAvailable || encoderOutputAvailable) {
                // Start by draining any pending output from the encoder.  It's important to
                // do this before we try to stuff any more data in.

                int encoderStatus = mEncoder.dequeueOutputBuffer(info, TIMEOUT_USEC);
                if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                    // no output available yet
                    if (VERBOSE) Log.d(TAG, "no output from encoder available in editVideoData");
                    encoderOutputAvailable = false;
                    Log.d(TAG,"decoder available : " + decoderOutputAvailable + ", encoder available: " + encoderOutputAvailable);
                } else if (encoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                    encoderOutputBuffers = mEncoder.getOutputBuffers();
                    if (VERBOSE) Log.d(TAG, "encoder output buffers changed");
                } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                    MediaFormat newFormat = mEncoder.getOutputFormat();

                    mTrackIndex = mMuxer.addTrack(newFormat);
                    mMuxer.start();
                    ismMuxerStarted = true;
                    if (VERBOSE) Log.d(TAG, "encoder output format changed: " + newFormat);
                } else if (encoderStatus < 0) {
                    fail("unexpected result from encoder.dequeueOutputBuffer: " + encoderStatus);
                } else { // encoderStatus >= 0
                    ByteBuffer encodedData = encoderOutputBuffers[encoderStatus];
                    if (encodedData == null) {
                        fail("encoderOutputBuffer " + encoderStatus + " was null");
                    }

                    // Write the data to the output "file".
                    if (info.size != 0) {
                        encodedData.position(info.offset);
                        encodedData.limit(info.offset + info.size);

                        mMuxer.writeSampleData(mTrackIndex, encodedData, info);

                        if (VERBOSE) Log.d(TAG, "encoder output " + info.size + " bytes");
                    }
                    outputDone = (info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0;
                    mEncoder.releaseOutputBuffer(encoderStatus, false);
                }
                if (encoderStatus != MediaCodec.INFO_TRY_AGAIN_LATER) {
                    // Continue attempts to drain output.
                    continue;
                }

                // Encoder is drained, check to see if we've got a new frame of output from
                // the decoder.  (The output is going to a Surface, rather than a ByteBuffer,
                // but we still get information through BufferInfo.)
                if (!eosReceived) {
                    int decoderStatus = mDecoder.dequeueOutputBuffer(info, TIMEOUT_USEC);
                    if (decoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                        // no output available yet
                        if (VERBOSE) Log.d(TAG, "no output from decoder available");
                        decoderOutputAvailable = false;
                    } else if (decoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                        //decoderOutputBuffers = decoder.getOutputBuffers();
                        if (VERBOSE) Log.d(TAG, "decoder output buffers changed (we don't care)");
                    } else if (decoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                        // expected before first buffer of data
                        MediaFormat newFormat = mDecoder.getOutputFormat();
                        if (VERBOSE) Log.d(TAG, "decoder output format changed: " + newFormat);
                    } else if (decoderStatus < 0) {
                        fail("unexpected result from decoder.dequeueOutputBuffer: "+decoderStatus);
                    } else { // decoderStatus >= 0
                        if (VERBOSE) Log.d(TAG, "surface decoder given buffer "
                                + decoderStatus + " (size=" + info.size + ")");
                        // The ByteBuffers are null references, but we still get a nonzero
                        // size for the decoded data.
                        boolean doRender = (info.size != 0);


                        // As soon as we call releaseOutputBuffer, the buffer will be forwarded
                        // to SurfaceTexture to convert to a texture.  The API doesn't
                        // guarantee that the texture will be available before the call
                        // returns, so we need to wait for the onFrameAvailable callback to
                        // fire.  If we don't wait, we risk rendering from the previous frame.
                        mDecoder.releaseOutputBuffer(decoderStatus, doRender);
                        if (doRender) {
                            // This waits for the image and renders it after it arrives.
                            if (VERBOSE) Log.d(TAG, "awaiting frame");
                            mOutputSurface.awaitNewImage();
                            mOutputSurface.drawImage();

                            // Send it to the encoder.
                            mInputSurface.setPresentationTime(info.presentationTimeUs * 1000);
                            if (VERBOSE) Log.d(TAG, "swapBuffers");
                            mInputSurface.swapBuffers();
                        }
                        if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                            // forward decoder EOS to encoder
                            if (VERBOSE) Log.d(TAG, "signaling input EOS");
                            if (WORK_AROUND_BUGS) {
                                // Bail early, possibly dropping a frame.
                                return;
                            } else {
                                decoderOutputAvailable=false;
                                eosReceived = true;
                                mEncoder.signalEndOfInputStream();
                            }
                        }
                    }
                }
            }
        }

        Log.d(TAG,"out of while in editVideoData");
        if (inputChunk != outputCount) {
            throw new RuntimeException("frame lost: " + inputChunk + " in, " +
                    outputCount + " out");
        }
    }

    public int InitDecodingIndex(boolean isFirstFile) {
        if(isFirstFile)
            return fileInd;
        if(fileInd < inputFileSize) {
            try {
                Log.d(TAG, "trying to init decoder for file index #"+fileInd);
                mExtractor = new MediaExtractor();
                mExtractor.setDataSource(filePaths[fileInd]);

                for (int i = 0; i < mExtractor.getTrackCount(); i++) {
                    inputFormat = mExtractor.getTrackFormat(i);
                    String mime = inputFormat.getString(MediaFormat.KEY_MIME);
                    if (mime.startsWith(VIDEO)) {
                        //!

                        if(!ismMuxerStarted) {
                            // Create an encoder format that matches the input format.  (Might be able to just
                            // re-use the format used to generate the video, since we want it to be the same.)
                            MediaFormat outputFormat = MediaFormat.createVideoFormat(MIME_TYPE, encWidth, encHeight);
                            outputFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                                    MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
                            outputFormat.setInteger(MediaFormat.KEY_BIT_RATE,
                                    125000);
                            outputFormat.setInteger(MediaFormat.KEY_FRAME_RATE,
                                    15);
                            outputFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL,
                                    5);

                            mEncoder = MediaCodec.createEncoderByType(MIME_TYPE);
                            mEncoder.configure(outputFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
                            mInputSurface = new InputSurface(mEncoder.createInputSurface());
                            mInputSurface.makeCurrent();
                            mEncoder.start();

                            Log.d(TAG, "encoder start, inputSurface makecurrent");
                            //ismMuxerStarted = true;
                        }
                        mExtractor.selectTrack(i);
                        mDecoder = MediaCodec.createDecoderByType(mime);
                        mOutputSurface = new OutputSurface();
                        //mOutputSurface.changeFragmentShader(FRAGMENT_SHADER);
                        try {
                            Log.d(TAG, "format : " + inputFormat);
                            mDecoder.configure(inputFormat, mOutputSurface.getSurface(), null, 0); //0:decoder
                        } catch (IllegalStateException e) {
                            Log.e(TAG, "codec '" + mime + "' failed configuration. " + e);
                            return -1;
                        }
                        mDecoder.start();
                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        fileInd++;

        return fileInd;
    }

    /**
     * Generates the presentation time for frame N, in microseconds.
     */
    private static long computePresentationTime(int frameIndex) {
        return 123 + frameIndex * 1000000 / 15;
    }


    private void releaseEncoder() {
        if (VERBOSE) Log.d(TAG, "releasing encoder objects");
        if (mEncoder != null) {
            Log.d(TAG,"release mEncoder");
            mEncoder.stop();
            mEncoder.release();
            mEncoder = null;
        }
        if (mInputSurface != null) {
            Log.d(TAG,"release mInputSurface");
            mInputSurface.release();
            mInputSurface = null;
        }

        if(ismMuxerStarted == true) {
            Log.d(TAG,"release mMuxer");
            mMuxer.stop();
            mMuxer.release();
            mMuxer = null;
        }

    }

    public void close() {
        eosReceived = true;
        releaseEncoder();
    }
}
