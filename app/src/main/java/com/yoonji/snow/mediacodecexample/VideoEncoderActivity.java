package com.yoonji.snow.mediacodecexample;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.File;

/**
 * Created by snow on 2017-07-27.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class VideoEncoderActivity extends Activity  {
    private final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 100;
    private VideoEncoderThread mVideoEncoder;
    private DecodeEditEncodeTest decodeEditEncodeTest;
    private static final String TAG = "VideoEncoderActivity";
    // where to put the output file (note: /sdcard requires WRITE_EXTERNAL_STORAGE permission)
    private static final File OUTPUT_DIR = Environment.getExternalStorageDirectory();
    private static final String[] FILE_PATH =
            {
                    "/storage/emulated/0/DCIM/Camera/20170727_201914.mp4",
                    //"/storage/emulated/0/test2.mp4",
                    //"/storage/emulated/0/DCIM/Camera/20170728_181624.mp4",
                    //"/storage/emulated/0/DCIM/Camera/20170728_181614.mp4",
                    //"/storage/emulated/0/Pictures/fotodanz/video/capture.mp4"
                    "/storage/emulated/0/test_yoonji_image.mp4"
            };
    //private static final String[] FILE_PATH = {"/storage/emulated/0/KakaoTalkDownload/KakaoTalk_Video_20160629_1631_24_484.mp4"};
    private DecodeEncodeThread decodeEncodeThread;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }



        decodeEditEncodeTest = new DecodeEditEncodeTest();
        decodeEditEncodeTest.init(FILE_PATH);
        decodeEditEncodeTest.run();
        try {
            decodeEditEncodeTest.join();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(decodeEditEncodeTest != null)
            decodeEditEncodeTest = null;
    }
}
